<?php

namespace Miniframe\Statistics\Service;

use Miniframe\Statistics\Model\PageHit;

class Storage
{
    /**
     * Precision constants for Storage->buildWhere()
     */
    protected const WHERE_PRECISION_MONTH = 1, WHERE_PRECISION_DAY = 2, WHERE_PRECISION_HOUR = 3;

    /**
     * Reference to the SQLite3 database.
     *
     * @var \SQLite3
     */
    protected $db;

    /**
     * Initializes the statistics storage
     *
     * @param string $storagePath Path to the storage folder.
     */
    public function __construct(string $storagePath)
    {
        $dbFile = rtrim($storagePath, '/') . '/statistics.db';
        $this->db = new \SQLite3($dbFile);
    }

    /**
     * Stores the hit in several tables
     *
     * @param PageHit $pageHit The page hit.
     *
     * @return void
     */
    public function store(PageHit $pageHit): void
    {
        $this->tableHitsPerHour($pageHit);
        $this->tableHitsPerPage($pageHit);
        $this->tableVisitorsPerCountry($pageHit);
        $this->tableSystemsPerMonth($pageHit);
    }

    /**
     * Populate the hits_per_hour table; that's 8.760 to 8.784 records per year
     *
     * @param PageHit $pageHit The page hit.
     *
     * @return void
     */
    protected function tableHitsPerHour(PageHit $pageHit): void
    {
        $year = $pageHit->getDateTime()->format("Y");
        $month = $pageHit->getDateTime()->format("n");
        $day = $pageHit->getDateTime()->format("j");
        $hour = $pageHit->getDateTime()->format("G");
        $weekday = $pageHit->getDateTime()->format("N");

        // Create table
        $this->db->query('CREATE TABLE IF NOT EXISTS "hits_per_hour" (
         	"year"     INTEGER NOT NULL,
         	"month"    INTEGER NOT NULL,
         	"day"      INTEGER NOT NULL,
         	"hour"     INTEGER NOT NULL,
         	"weekday"  INTEGER NOT NULL,
         	"hits"     INTEGER NOT NULL,
         	"visitors" INTEGER NOT NULL,
         	CONSTRAINT "date" PRIMARY KEY("year","month","day","hour")
        )');

        // Create record for the date
        $this->db->query('INSERT OR IGNORE INTO "hits_per_hour" VALUES (
            ' . $year . ', ' . $month . ', ' . $day . ', ' . $hour . ', ' . $weekday . ', 0, 0
        )');

        // Increment correct values
        $this->db->query('UPDATE `hits_per_hour` SET 
            `hits` = `hits` + 1
            ' . ($pageHit->isNewVisitor() ? ', `visitors` = `visitors` + 1' : '') . '
        WHERE 
            `year` = ' . $year . ' AND `month` = ' . $month . ' AND `day` = ' . $day . ' AND `hour` = ' . $hour . '
        ');
    }

    /**
     * Populate the hits_per_page table
     *
     * @param PageHit $pageHit The page hit.
     *
     * @return void
     */
    protected function tableHitsPerPage(PageHit $pageHit): void
    {
        $year = $pageHit->getDateTime()->format("Y");
        $month = $pageHit->getDateTime()->format("n");
        $day = $pageHit->getDateTime()->format("j");
        $weekday = $pageHit->getDateTime()->format("N");
        $page = addslashes(parse_url($pageHit->getRequestUri(), PHP_URL_PATH)); // Remove query part

        // Create table
        $this->db->query('CREATE TABLE IF NOT EXISTS "hits_per_page" (
         	"year"    INTEGER NOT NULL,
         	"month"   INTEGER NOT NULL,
         	"day"     INTEGER NOT NULL,
         	"weekday" INTEGER NOT NULL,
         	"page"    TEXT NOT NULL,
         	"hits"    INTEGER NOT NULL,
         	CONSTRAINT "date" PRIMARY KEY("year","month","day","page")
        )');

        // Create record for the date
        $this->db->query('INSERT OR IGNORE INTO "hits_per_page" VALUES (
            ' . $year . ', ' . $month . ', ' . $day . ', ' . $weekday . ', "' . $page . '", 0
        )');

        // Increment correct value
        $this->db->query('UPDATE `hits_per_page` SET 
            `hits` = `hits` + 1
        WHERE 
            `year` = ' . $year . ' AND `month` = ' . $month . ' AND `day` = ' . $day . ' AND `page` = "' . $page . '"
        ');
    }

    /**
     * Populate the visitors_per_country table
     *
     * @param PageHit $pageHit The page hit.
     *
     * @return void
     */
    protected function tableVisitorsPerCountry(PageHit $pageHit): void
    {
        // Only log unique visitors
        if (!$pageHit->isNewVisitor()) {
            return;
        }

        $year = $pageHit->getDateTime()->format("Y");
        $month = $pageHit->getDateTime()->format("n");
        $day = $pageHit->getDateTime()->format("j");
        $hour = $pageHit->getDateTime()->format("G");
        $weekday = $pageHit->getDateTime()->format("N");
        $country = addslashes($pageHit->getCountry() ?? '--');

        // Create table
        $this->db->query('CREATE TABLE IF NOT EXISTS "visitors_per_country" (
            "year"     INTEGER NOT NULL,
            "month"    INTEGER NOT NULL,
            "day"      INTEGER NOT NULL,
            "hour"     INTEGER NOT NULL,
            "weekday"  INTEGER NOT NULL,
            "country"  TEXT NOT NULL,
            "visitors" INTEGER NOT NULL,
            PRIMARY KEY("year","month","day","hour","country")
        )');

        // Create record for the date + country
        $this->db->query('INSERT OR IGNORE INTO "visitors_per_country" VALUES (
            ' . $year . ', ' . $month . ', ' . $day . ', ' . $hour . ', ' . $weekday . ', "' . $country . '", 0
        )');

        // Increment correct value
        $this->db->query('UPDATE `visitors_per_country` SET 
            `visitors` = `visitors` + 1
        WHERE 
            `year` = ' . $year . ' AND `month` = ' . $month . ' AND `day` = ' . $day . ' AND `hour` = ' . $hour
            . ' AND `country` = "' . $country . '"
        ');
    }

    /**
     * Populate the systems_per_month table
     *
     * @param PageHit $pageHit The page hit.
     *
     * @return void
     */
    protected function tableSystemsPerMonth(PageHit $pageHit): void
    {
        // Only log unique visitors
        if (!$pageHit->isNewVisitor()) {
            return;
        }

        $year = $pageHit->getDateTime()->format("Y");
        $month = $pageHit->getDateTime()->format("n");
        $browser = addslashes($pageHit->getBrowserBrand() ?? '--');
        $version = addslashes($pageHit->getBrowserVersion() ?? '--');
        $platform = addslashes($pageHit->getPlatform() ?? '--');
        $deviceType = addslashes($pageHit->getDeviceType() ?? '--');

        // Create table
        $this->db->query('CREATE TABLE IF NOT EXISTS "systems_per_month" (
            "year"        INTEGER NOT NULL,
            "month"       INTEGER NOT NULL,
            "browser"     TEXT NOT NULL,
            "version"     TEXT NOT NULL,
            "platform"    TEXT NOT NULL,
            "device_type" TEXT NOT NULL,
            "visitors"    INTEGER NOT NULL,
            PRIMARY KEY("year","month","browser","version","platform","device_type")
        )');

        // Create record for the date + system
        $this->db->query('INSERT OR IGNORE INTO "systems_per_month" VALUES (
            ' . $year . ', ' . $month
            . ', "' . $browser . '", "' . $version . '", "' . $platform . '", "' . $deviceType . '", 0
        )');

        // Increment correct value
        $this->db->query('UPDATE `systems_per_month` SET 
            `visitors` = `visitors` + 1
        WHERE 
            `year` = ' . $year . ' AND `month` = ' . $month . ' AND `browser` = "' . $browser . '" AND `version` = "'
            . $version . '" AND `platform` = "' . $platform . '" AND `device_type` = "' . $deviceType . '"
        ');
    }

    /**
     * Returns the amount of hits and unique visitors per day between two dates.
     *
     * @param \DateTime $from Start date.
     * @param \DateTime $till End date.
     *
     * @return array
     */
    public function getHitsPerDay(\DateTime $from, \DateTime $till): array
    {
        // Build and execute query
        $query = 'SELECT `year`, `month`, `day`, SUM(`hits`), SUM(`visitors`) FROM `hits_per_hour`
            WHERE ' . $this->buildWhere($from, $till, static::WHERE_PRECISION_DAY) . '
            GROUP BY `year`, `month`, `day`
        ;';
        $result = $this->db->query($query);

        // Convert result in a return array
        $return = array();
        while ($row = $result->fetchArray()) {
            $date = date('Y-m-d', mktime(0, 0, 0, $row[1], $row[2], $row[0]));
            $return[] = ['date' => $date, 'hits' => $row[3], 'visitors' => $row[4]];
        }

        return $return;
    }

    /**
     * Returns the amount of hits per hour and weekday between two dates.
     *
     * @param \DateTime $from Start date.
     * @param \DateTime $till End date.
     *
     * @return array
     */
    public function getHitsPerHourPerWeekday(\DateTime $from, \DateTime $till): array
    {
        // Build and execute query
        $query = 'SELECT `weekday`, `hour`, SUM(`hits`) FROM `hits_per_hour`
            WHERE ' . $this->buildWhere($from, $till, static::WHERE_PRECISION_DAY) . '
            GROUP BY `weekday`, `hour`
        ;';
        $result = $this->db->query($query);

        // Convert result in a return array
        $return = array();
        while ($row = $result->fetchArray()) {
            $return[$row[0]][$row[1]] = $row[2];
        }

        return $return;
    }

    /**
     * Returns the amount of hits per page
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getHitsPerPage(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        // Build and execute query
        $query = 'SELECT `page`, SUM(`hits`) AS `total` FROM `hits_per_page`
            WHERE ' . $this->buildWhere($from, $till, static::WHERE_PRECISION_DAY) . '
            GROUP BY `page`
            ORDER BY `total` DESC
            LIMIT ' . $limit . '
        ;';
        $result = $this->db->query($query);

        // Convert result in a return array
        $return = array();
        while ($row = $result->fetchArray()) {
            $return[] = ['page' => $row[0], 'hits' => $row[1]];
        }

        return $return;
    }

    /**
     * Returns the amount of visitors per country
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getVisitorsPerCountry(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        // Build and execute query
        $query = 'SELECT `country`, SUM(`visitors`) AS `total` FROM `visitors_per_country`
            WHERE ' . $this->buildWhere($from, $till, static::WHERE_PRECISION_DAY) . '
            GROUP BY `country`
            ORDER BY `total` DESC
            LIMIT ' . $limit . '
        ;';
        $result = $this->db->query($query);

        // Convert result in a return array
        $return = array();
        while ($row = $result->fetchArray()) {
            $return[] = ['country' => $row[0], 'visitors' => $row[1]];
        }

        return $return;
    }

    /**
     * Returns the amount of visitors per platform
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getVisitorsPerPlatform(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        return $this->getVisitorsPerSystem('platform', $from, $till, $limit);
    }

    /**
     * Returns the amount of visitors per browser
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getVisitorsPerBrowser(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        return $this->getVisitorsPerSystem('browser', $from, $till, $limit);
    }

    /**
     * Returns the amount of visitors per browser version
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getVisitorsPerBrowserVersion(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        return $this->getVisitorsPerSystem('version', $from, $till, $limit);
    }

    /**
     * Returns the amount of visitors per device type
     *
     * @param \DateTime $from  Start date.
     * @param \DateTime $till  End date.
     * @param integer   $limit The max. rows to return.
     *
     * @return array
     */
    public function getVisitorsPerDeviceType(\DateTime $from, \DateTime $till, int $limit = 100): array
    {
        return $this->getVisitorsPerSystem('device_type', $from, $till, $limit);
    }

    /**
     * Returns the amount of visitors per system
     *
     * @param string    $column Column to zoom in on.
     * @param \DateTime $from   Start date.
     * @param \DateTime $till   End date.
     * @param integer   $limit  The max. rows to return.
     *
     * @return array
     */
    protected function getVisitorsPerSystem(string $column, \DateTime $from, \DateTime $till, int $limit): array
    {
        if ($column == 'version') {
            $columnString = "`browser`, `version`";
        } else {
            $columnString = "`" . $column . "`";
        }

        // Build and execute query
        $query = 'SELECT ' . $columnString . ', SUM(`visitors`) AS `total` FROM `systems_per_month`
            WHERE ' . $this->buildWhere($from, $till, static::WHERE_PRECISION_MONTH) . '
            GROUP BY ' . $columnString . '
            ORDER BY `total` DESC
            LIMIT ' . $limit . '
        ;';
        $result = $this->db->query($query);

        // Convert result in a return array
        $return = array();
        while ($row = $result->fetchArray()) {
            if ($column == 'version') {
                $return[] = ['browser_version' => $row[0] . ' ' . $row[1], 'visitors' => $row[2]];
            } else {
                $return[] = [$column => $row[0], 'visitors' => $row[1]];
            }
        }

        return $return;
    }

    /**
     * Builds a WHERE statement between two dates.
     *
     * @param \DateTime $from      The start date.
     * @param \DateTime $till      The end date.
     * @param integer   $precision The precision (WHERE_PRECISION_MONTH, WHERE_PRECISION_DAY, WHERE_PRECISION_HOUR).
     *
     * @return string
     */
    protected function buildWhere(\DateTime $from, \DateTime $till, int $precision): string
    {
        $fromYear = $from->format("Y");
        $fromMonth = $from->format("n");
        $fromDay = $from->format("j");
        $fromHour = $from->format("G");
        $tillYear = $till->format("Y");
        $tillMonth = $till->format("n");
        $tillDay = $till->format("j");
        $tillHour = $till->format("G");

        $fromWhere  = '"year" > ' . $fromYear;
        $tillWhere  = '"year" < ' . $tillYear;

        if ($precision == static::WHERE_PRECISION_MONTH) {
            $fromWhere .= ' OR ("year" = ' . $fromYear . ' AND "month" >= ' . $fromMonth . ')';
            $tillWhere .= ' OR ("year" = ' . $tillYear . ' AND "month" <= ' . $tillMonth . ')';
        } else {
            $fromWhere .= ' OR ("year" = ' . $fromYear . ' AND "month" > ' . $fromMonth . ')';
            $tillWhere .= ' OR ("year" = ' . $tillYear . ' AND "month" < ' . $tillMonth . ')';
            if ($precision == static::WHERE_PRECISION_DAY) {
                $fromWhere .= ' OR ("year" = ' . $fromYear . ' AND "month" = ' . $fromMonth . ' AND "day" >= '
                    . $fromDay . ')';
                $tillWhere .= ' OR ("year" = ' . $tillYear . ' AND "month" = ' . $tillMonth . ' AND "day" <= '
                    . $tillDay . ')';
            } else {
                $fromWhere .= ' OR ("year" = ' . $fromYear . ' AND "month" = ' . $fromMonth . ' AND "day" > '
                    . $fromDay . ')';
                $fromWhere .= ' OR ("year" = ' . $fromYear . ' AND "month" = ' . $fromMonth . ' AND "day" = '
                    . $fromDay . ' AND "hour" >= ' . $fromHour . ')';
                $tillWhere .= ' OR ("year" = ' . $tillYear . ' AND "month" = ' . $tillMonth . ' AND "day" < '
                    . $tillDay . ')';
                $tillWhere .= ' OR ("year" = ' . $tillYear . ' AND "month" = ' . $tillMonth . ' AND "day" = '
                    . $tillDay . ' AND "hour" <= ' . $tillHour . ')';
            }
        }

        return '(' . $fromWhere . ') AND (' . $tillWhere . ')';
    }
}
