<?php

namespace Miniframe\Statistics\Service;

class GeoipUpdater
{
    /**
     * Maxmind license key.
     *
     * @var string
     */
    protected $licenseKey;
    /**
     * Storage folder for database files.
     *
     * @var string
     */
    protected $databasePath;
    /**
     * URL for the API
     *
     * @var string
     */
    protected $urlApi = 'https://download.maxmind.com/app/geoip_download';

    /**
     * Initiates the GeoIP database Updater
     *
     * @param string $licenseKey   Maxmind license key.
     * @param string $databasePath Storage folder for database files.
     */
    public function __construct(string $licenseKey, string $databasePath)
    {
        $this->licenseKey = $licenseKey;
        $this->databasePath = $databasePath;
    }

    /**
     * Makes sure we've got a database and returns the full path to the database
     *
     * @return void
     */
    public function getDatabase(): void
    {
        if (!is_dir($this->databasePath)) {
            mkdir($this->databasePath, 0777, true);
        }
        $edition = 'GeoLite2-Country';
        $suffix = 'tar.gz';
        $tarGzFile = $this->databasePath . '/' . $edition . '.' . $suffix;
        $tarFile = $this->databasePath . '/' . $edition . '.tar';
        $mmdbFile = $this->databasePath . '/' . $edition . '.mmdb';

        // Step 1: Download .tar.gz file
        $fh = fopen($tarGzFile, 'wb');
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $this->urlApi . '?' . http_build_query([
                    'edition_id' => $edition,
                    'suffix' => $suffix,
                    'license_key' => $this->licenseKey,
                ]),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTPGET => true,
            CURLOPT_HEADER => false,
            CURLOPT_FILE => $fh,
        ]);
        curl_exec($ch);
        curl_close($ch);
        fclose($fh);

        // Step 2: unzip .tar.gz file
        if (file_exists($tarFile)) {
            unlink($tarFile);
        }
        $ungzip = new \PharData($tarGzFile);
        $ungzip->decompress();

        // Step 3: unpack .tar file
        $untar = new \PharData($tarFile);
        $untar->extractTo($this->databasePath);
        foreach (glob($this->databasePath . '/*', GLOB_ONLYDIR) as $dir) {
            if (substr(pathinfo($dir, PATHINFO_BASENAME), 0, strlen($edition)) == $edition) {
                foreach (glob($dir . '/*') as $file) {
                    $dest = $this->databasePath . '/' . pathinfo($file, PATHINFO_BASENAME);
                    if (file_exists($dest)) {
                        unlink($dest);
                    }
                    rename($file, $dest);
                }
                rmdir($dir);
                break;
            }
        }
    }
}
