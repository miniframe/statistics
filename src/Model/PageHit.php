<?php

namespace Miniframe\Statistics\Model;

class PageHit
{
    /**
     * Date/time of the hit
     *
     * @var \DateTime
     */
    protected $dateTime;
    /**
     * True when this is the 1st page hit for this user
     *
     * @var bool
     */
    protected $newVisitor;
    /**
     * Requested URL
     *
     * @var string
     */
    protected $requestUri;
    /**
     * Country code
     *
     * @var string|null
     */
    protected $country;
    /**
     * Browser brand
     *
     * @var string|null
     */
    protected $browserBrand;
    /**
     * Browser version
     *
     * @var string|null
     */
    protected $browserVersion;
    /**
     * Platform
     *
     * @var string|null
     */
    protected $platform;
    /**
     * Device type
     *
     * @var string|null
     */
    protected $deviceType;

    /**
     * Creates a new PageHit data model
     *
     * @param \DateTime   $dateTime       Date/time of the hit.
     * @param boolean     $newVisitor     True when this is the 1st page hit for this user.
     * @param string      $requestUri     Requested URL.
     * @param string|null $country        Country code.
     * @param string|null $browserBrand   Browser brand.
     * @param string|null $browserVersion Browser version.
     * @param string|null $platform       Platform.
     * @param string|null $deviceType     Device type.
     */
    public function __construct(
        \DateTime $dateTime,
        bool $newVisitor,
        string $requestUri,
        ?string $country,
        ?string $browserBrand,
        ?string $browserVersion,
        ?string $platform,
        ?string $deviceType
    ) {
        $this->dateTime = $dateTime;
        $this->newVisitor = $newVisitor;
        $this->requestUri = $requestUri;
        $this->country = $country;
        $this->browserBrand = $browserBrand;
        $this->browserVersion = $browserVersion;
        $this->platform = $platform;
        $this->deviceType = $deviceType;
    }

    /**
     * Returns the Date/time of the hit
     *
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * Returns true when this is the 1st page hit for this user
     *
     * @return boolean
     */
    public function isNewVisitor(): bool
    {
        return $this->newVisitor;
    }

    /**
     * Returns the Requested URL
     *
     * @return string
     */
    public function getRequestUri(): string
    {
        return $this->requestUri;
    }

    /**
     * Returns the Country code
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * Returns the Browser brand
     *
     * @return string|null
     */
    public function getBrowserBrand(): ?string
    {
        return $this->browserBrand;
    }

    /**
     * Returns the Browser version
     *
     * @return string|null
     */
    public function getBrowserVersion(): ?string
    {
        return $this->browserVersion;
    }

    /**
     * Returns the Platform
     *
     * @return string|null
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    /**
     * Returns the Device type
     *
     * @return string|null
     */
    public function getDeviceType(): ?string
    {
        return $this->deviceType;
    }

    /**
     * Returns a pagehit object with a specific state (makes this object serializable with var_export)
     *
     * @param array $properties The list of properties.
     *
     * @return PageHit
     */
    public static function __set_state(array $properties): self
    {
        return new self(
            $properties['dateTime'],
            $properties['newVisitor'],
            $properties['requestUri'],
            $properties['country'],
            $properties['browserBrand'],
            $properties['browserVersion'],
            $properties['platform'],
            $properties['deviceType']
        );
    }
}
