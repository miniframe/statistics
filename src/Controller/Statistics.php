<?php

namespace Miniframe\Statistics\Controller;

use GeoIp2\Database\Reader;
use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\ForbiddenResponse;
use Miniframe\Statistics\Service\Browscap;
use Miniframe\Statistics\Service\GeoipUpdater;
use Miniframe\Statistics\Service\Storage;

class Statistics extends AbstractController
{
    /**
     * Reference to statistics storage
     *
     * @var Storage
     */
    protected $storage;

    /**
     * Initializes the Statistics Controller
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        $this->storage = new Storage($config->getPath('statistics', 'storage_path'));
    }

    /**
     * Returns the main page
     *
     * @return Response
     */
    public function main(): Response
    {
        $from = new \DateTime();
        $from->modify('-1 year');
        $till = new \DateTime();
        $hitsPerDay = $this->storage->getHitsPerDay($from, $till);
        $hitsPerHourPerWeekday = $this->storage->getHitsPerHourPerWeekday($from, $till);
        $hitsPerPage = $this->storage->getHitsPerPage($from, $till, 25);
        $visitorsPerCountry = $this->storage->getVisitorsPerCountry($from, $till, 25);
        $visitorsPerPlatform = $this->storage->getVisitorsPerPlatform($from, $till, 25);
        $visitorsPerBrowser = $this->storage->getVisitorsPerBrowser($from, $till, 25);
        $visitorsPerBrowserVersion = $this->storage->getVisitorsPerBrowserVersion($from, $till, 25);
        $visitorsPerDeviceType = $this->storage->getVisitorsPerDeviceType($from, $till, 25);

        return new PhpResponse(__DIR__ . '/../../templates/statistics.html.php', [
            'hitsPerDay'                => $hitsPerDay,
            'hitsPerHourPerWeekday'     => $hitsPerHourPerWeekday,
            'hitsPerPage'               => $hitsPerPage,
            'visitorsPerCountry'        => $visitorsPerCountry,
            'visitorsPerPlatform'       => $visitorsPerPlatform,
            'visitorsPerBrowser'        => $visitorsPerBrowser,
            'visitorsPerBrowserVersion' => $visitorsPerBrowserVersion,
            'visitorsPerDeviceType'     => $visitorsPerDeviceType,
        ]);
    }

    /**
     * This method responds to `php public/index.php _STATISTICS update`
     *
     * @return Response
     */
    public function update(): Response
    {
        if (!$this->request->isShellRequest()) {
            throw new ForbiddenResponse();
        }

        // Update and configure browscap
        echo 'Updating browscap.ini file...' . PHP_EOL;
        flush();

        $browscap = new Browscap($this->config->getPath('statistics', 'browscap_database_path'));
        $browscap->getDatabase();

        // When geoip2/geoip2 is installed, update database files
        if (class_exists(Reader::class)) {
            echo 'Updating GeoIP database...' . PHP_EOL;
            flush();

            $updater = new GeoipUpdater(
                $this->config->get('statistics', 'geoip_licensekey'),
                $this->config->getPath('statistics', 'geoip_database_path')
            );
            $updater->getDatabase();
        } else {
            echo 'Skipping GeoIP database' . PHP_EOL;
            flush();
        }

        return new Response('Databases updated' . PHP_EOL);
    }
}
