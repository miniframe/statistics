<?php

namespace Miniframe\Statistics\Middleware;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Statistics\Model\PageHit;
use Miniframe\Statistics\Service\Browscap;
use Miniframe\Statistics\Service\Storage;
use Miniframe\Toolbar\Service\DeveloperToolbar;
use Miniframe\Statistics\Controller\Statistics as StatisticsController;

class Statistics extends AbstractMiddleware
{
    /**
     * Path to the country database
     *
     * @var string|null
     */
    protected $CountryMmdbFile;

    /**
     * Reference to the Browscap class
     *
     * @var Browscap
     */
    protected $browscap;

    /**
     * Reference to the storage for statistics.
     *
     * @var Storage
     */
    protected $storage;

    /**
     * URL prefix for statistic pages
     *
     * @var string
     */
    protected $statisticsPrefix = '_STATISTICS';

    /**
     * Set to true to ignore collecting data
     *
     * @var bool
     */
    protected $ignoreCollection = false;

    /**
     * Initiates the Statistics middleware
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // If there's a slug set, overwrite the current prefix
        if ($config->has('statistics', 'slug')) {
            $this->statisticsPrefix = $config->get('statistics', 'slug');
            if (!preg_match('/^[a-z0-9\_\-]+$/i', $this->statisticsPrefix)) {
                throw new \InvalidArgumentException('Slug has illegal characters');
            }
        }

        // We won't log console requests
        if ($this->request->isShellRequest()) {
            $this->ignoreCollection = true;
            return;
        }

        $this->storage = new Storage($config->getPath('statistics', 'storage_path'));

        // Configure and validate GeoIP database
        if (class_exists(Reader::class) && $config->has('statistics', 'geoip_database_path')) {
            $this->CountryMmdbFile = $config->getPath('statistics', 'geoip_database_path')
                . '/GeoLite2-Country.mmdb';
            if (!file_exists($this->CountryMmdbFile)) {
                throw new \RuntimeException('No geoip database found. Please run the update command');
            }
        }

        // Validate browscap database
        $path = $this->config->getPath('statistics', 'browscap_database_path');
        $this->browscap = new Browscap($path);
        if (!ini_get('browscap')) {
            if (!file_exists($path . '/browscap.php')) {
                throw new \RuntimeException('No browscap database found. Please run the update command');
            }
        }
    }

    /**
     * Handles the statistics pages
     *
     * For a good example, see ../Middleware/UrlToMvcRouter.php
     *
     * @return callable[]
     */
    public function getRouters(): array
    {
        return [function () {
            $path = $this->request->getPath();

            // Remove base path, if present
            $basePath = explode('/', trim(parse_url($this->config->get('framework', 'base_href'), PHP_URL_PATH), '/'));
            foreach ($basePath as $prefix) {
                if (isset($path[0]) && $path[0] == $prefix) {
                    array_shift($path);
                    $path = array_values($path); // Reset indexes
                }
            }

            if (count($path) == 1 && $path[0] == $this->statisticsPrefix) {
                $this->ignoreCollection = true;
                return [new StatisticsController($this->request, $this->config), 'main'];
            } elseif (
                count($path) > 1
                && $path[0] == $this->statisticsPrefix
                && method_exists(StatisticsController::class, $path[1])
            ) {
                $this->ignoreCollection = true;
                return [new StatisticsController($this->request, $this->config), $path[1]];
            }

            return null;
        }];
    }

    /**
     * Adds the post processor that triggers data logging
     *
     * @return callable[]
     */
    public function getPostProcessors(): array
    {
        return [function (Response $response): Response {
            // Collecting can be ignored by request
            if ($this->ignoreCollection) {
                return $response;
            }

            try {
                $pageHit = $this->collectData($response);
                if ($pageHit) {
                    $this->storage->store($pageHit);
                }
            } catch (\Throwable $throwable) {
                // Ignore; we don't want to break the site for statistical purposes
                if (class_exists(DeveloperToolbar::class)) {
                    DeveloperToolbar::getInstance()->dump([
                        $throwable->getMessage(), $throwable->getFile(), $throwable->getLine()
                    ]);
                }
            }
            return $response;
        }];
    }

    /**
     * Collects data based on the Response class and the available Request object
     *
     * @param Response $response The response object.
     *
     * @return PageHit|null
     */
    protected function collectData(Response $response): ?PageHit
    {
        if ($response->getResponseCode() != 200) {
            return null;
        }

        $dateTime = new \DateTime();
        $requestUri = $this->request->getServer('REQUEST_URI');
        $userAgent = $this->request->getServer('HTTP_USER_AGENT');
        $remoteAddress = $this->request->getServer('REMOTE_ADDR');
        $country = $this->getCountryByIp($remoteAddress);
        $browserInfo = $this->browscap->getBrowser($userAgent);
        $browserBrand = $browserInfo['Browser'] ?? null;
        $browserVersion = $browserInfo['Version'] ?? null;
        $platform = $browserInfo['Platform'] ?? null;
        $deviceType = $browserInfo['Device_Type'] ?? null;

        // When the referrer has the same hostname as the current domain, it's not a new visitor
        $newVisitor = true;
        $referrer = $this->request->getServer('HTTP_REFERER');
        if ($referrer !== null) {
            $referrerHost = parse_url($referrer, PHP_URL_HOST);
            $serverHost = $this->request->getServer('HTTP_HOST');
            if ($referrerHost == $serverHost) {
                $newVisitor = false;
            }
        }

        // Remove data from memory; we are not going to process this info, and its personal data.
        $remoteAddress = null;
        $userAgent = null;
        $browserInfo = null;
        $referrer = null;

        return new PageHit(
            $dateTime,
            $newVisitor,
            $requestUri,
            $country,
            $browserBrand,
            $browserVersion,
            $platform,
            $deviceType
        );
    }

    /**
     * Returns the ISO code of a country (or null) for a specific IP address
     *
     * @param string $ip The IP address.
     *
     * @return string|null
     */
    protected function getCountryByIp(string $ip): ?string
    {
        // Fetch country code based on IP address
        if (class_exists(Reader::class) && $this->CountryMmdbFile) {
            $reader = new Reader($this->CountryMmdbFile);
            try {
                return $reader->country($ip)->country->isoCode;
            } catch (AddressNotFoundException $exception) {
                // Ignore, returning null in the end
            }
        }
        return null;
    }
}
