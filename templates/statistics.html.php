<?php

/* @var $hitsPerDay array */
/* @var $hitsPerHourPerWeekday array */
/* @var $hitsPerPage array */
/* @var $visitorsPerCountry array */
/* @var $visitorsPerPlatform array */
/* @var $visitorsPerBrowser array */
/* @var $visitorsPerBrowserVersion array */
/* @var $visitorsPerDeviceType array */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <title>Website statistics</title>
</head>
<body>
<div class="container">
    <div class="row">

        <div class="col-sm-6">

            <table class="table">
                <thead>
                    <tr><th>Date</th><th>Hits</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($hitsPerDay as $data) : ?>
                    <tr><td><?= $data['date'] ?></td><td><?= $data['hits'] ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>&nbsp;</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th></tr>
                </thead>
                <tbody>
                <?php for ($hour = 0; $hour < 24; ++$hour) : ?>
                    <tr>
                        <td><?= $hour ?></td>
                        <?php for ($weekday = 1; $weekday < 8; ++$weekday) : ?>
                            <td><?= $hitsPerHourPerWeekday[$weekday][$hour] ?? 0 ?></td>
                        <?php endfor; ?>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>

        </div>
        <div class="col-sm-6">

            <table class="table">
                <thead>
                <tr><th>Page</th><th>Hits</th></tr>
                </thead>
                <tbody>
                <?php foreach ($hitsPerPage as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['page']) ?></td><td><?= $data['hits'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>Country</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($visitorsPerCountry as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['country']) ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>Platform</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($visitorsPerPlatform as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['platform']) ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>Browser</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($visitorsPerBrowser as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['browser']) ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>Browser version</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($visitorsPerBrowserVersion as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['browser_version']) ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr><th>Device type</th><th>Visitors</th></tr>
                </thead>
                <tbody>
                <?php foreach ($visitorsPerDeviceType as $data) : ?>
                    <tr><td><?= htmlspecialchars($data['device_type']) ?></td><td><?= $data['visitors'] ?></td></tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </div>

    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</body>
</html>
