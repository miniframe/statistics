# Miniframe Website statistics

This package adds website statistics to the [Miniframe PHP Framework](https://miniframe.dev/).

[![build](https://miniframe.dev/build/badge/statistics)](https://bitbucket.org/miniframe/statistics/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/statistics)](https://miniframe.dev/codecoverage/statistics)

## How to use

1. In your existing project, type: `composer require miniframe/statistics`
2. Add the directive below to your config.ini
3. Run the task `php public/index.php _STATISTICS update` to update geoip and browscap databases
4. Visit `yourwebsite/_STATISTICS` for output

*Example config.ini directives*
```ini
[framework]
middleware[] = Miniframe\Statistics\Middleware\Statistics

[statistics]
browscap_database_path = cache/browscap
storage_path = cache/
```

It's recommended to run the update task on a regular base.

## How to enable GeoIP (country lookup)

For GeoIP, I use a local database so the IP address is not shared with any 3rd party.
To get this database, a few steps are required;

1. Add in your configuration ini:
   ```ini
   [statistics]
   geoip_database_path = cache/geoip
   ```
2. Type `composer require geoip2/geoip2:~2.0`
3. Create an account at https://dev.maxmind.com/geoip/geolite2-free-geolocation-data
4. When you have an account, create a new license key at https://www.maxmind.com/en/accounts/613599/license-key and check no for the update tool.
5. Copy the `License Key` and put them in a `secrets.ini`:  
   ```ini
   [statistics]
   geoip_licensekey = ****************
   ```  
   It's also recommended to put the `secrets.ini` in your `.gitignore` file so it won't end up in a git repository.
6. Run the task `php public/index.php _STATISTICS update` to update the geoip database.

You can also install a tool to automatically download the databases.

## How to password protect your statistics

For password protection, you can use the [Basic HTTP Authentication](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/BasicAuthentication.md) middleware.
For more about that, see the middlewares [documentation](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/BasicAuthentication.md).

## How to change the URL for statistics

You can modify the URL for the statistics page by altering the slug:

```ini
[statistics]
slug = stats
```


## GDPR compliance

First, let me state this is not covered by a lawyer, no garantees here.  
I wrote this library to try to be fully complaint with the GDPR.
As far as my knowledge goes, no personal data is stored.

I only store some global data like a country code, operating system and browser version,
and don't even link between those data sets since there is no real need for that.

All data actually used can be seen in [src/Model/PageHit.php](src/Model/PageHit.php)

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/statistics/addon/pipelines/home) to automatically test this project.
